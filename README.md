# Landbell Challenge
**PHP developer recruitment.**

### Overview

There are 2 different challenges, Users and Books, you can use the proposed structure below or proposed a new one.

├───Challenge_(Books or Users)

│   ├───public

│   │   ├───css

│   │   └───js

│   └───src


**User challenge is mandatory and Books challenge is a plus =D**

---

### What we look for
This task is designed to give us an idea of how you approach programming problems. 

We will look at the structure of any database tables you create, as well as the structure of your code.

We are looking at how efficient your algorithms are and also how you design the html and/or any javascript you may use.

---

### Challenges
**1º - Users**

 - You must develop the form suggested in the mockup provided (mockup-user-form.png), the form developed should replicate exactly the one present in the given reference(mockup-user-form.png).
     Follow the validation: 
   
   	- Upon form fulfilment, a message must be presented stating if it was a success or indication
possible problems.
   
  	- A check for email already in use must be implemented **without the need to submit the form**.
   	- NIF  Nine numeric digits
   	- Postal Code  XXXX – XXX
    - Phone  If Country is Portugal should only allow PT phone numbers
    - Password strength algorithm can be defined by the candidate and explained in code comments.
    - Security and code efficiency are important evaluation points.
    - Colors/Design messages is not closed, you can create what you are able.
    - The country list can be filled only with 4 countries (PT, ES, FR, DE)
  
 - The data received by the form must be saved on DB, you are free to design the schema.
 - Create users listing page including only the fields: Id, Nome, Apelido, E-mail, País e Localidade

**2º - Books** 
 - You must develop a listing page all books given reference(books.xml) including the fields: id, title, author, desc, lprice e cprice.
 - Add a filter by price.(sample between min and max price)

---

### Requirements
 - Use PHP ***WITHOUT ANY*** frameworks.
 - Use javascript libraries if you wish.
 - Your implementation must work with MySql or MariaDb. 
 - Include a short documentation of ***how to install***.
 
 
 We look forward to seeing your work =)

